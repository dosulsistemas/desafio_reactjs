# O trabalho por aqui...

Que tal fazer parte do nosso time de devs e participar de grandes projetos?  
Ter a oportunidade de trabalhar com tecnologias e conceitos inovadores, como:

- ReactJS
- React Native
- JavaScript
- Magento
- PHP
- Versionamento Git
- Ambiente Web
- SQL

# Tecnologias/Padrões

Tecnologia exigida para o teste:

- ReactJS

# O desafio - Escolha um dos desafios

## 1 - Crie uma Single Page Application listando perfis de usuários do GitHub, podendo eles serem seus seguidores, ou pessoas que você segue.

API: [https://developer.github.com/v3/](https://developer.github.com/v3/)

O App consiste em uma tela que lista os usuários.

- (OPCIONAL) Criar uma tela para visualizar os detalhes básicos do perfil clicado.

## 2 - Crie uma Single Page Application de uma loja de quadrinhos utilizando a API do Star Wars.

API: [https://swapi.co/](https://swapi.co/)

Faça uma página com uma listagem de no máximo 6 filmes por página.

- (OPCIONAL) Uma página de detalhe do filme com dados de personagens que participaram do filme.

# Prazo

Não existe prazo para a sua entrega!

# Complemento

Qualquer funcionalidade extra é bem vinda para agregar na solução básica proposta.

O layout é por sua conta, seja criativo.
A arquitetura é por sua conta, seja esperto.

Você será avaliado pela qualidade do código, pela modularidade, pela legibilidade, pela criatividade, pela quantidade de funcionalidades básicas e extra.

Não esqueça da otimização de velocidade da App nem da experiência do usuário.

(OBS: O desafio deve ser feito em um repositório público na sua conta do GitHub. Caso não tenha uma conta no GitHub, deverá ser criado.)

# Boa sorte !!!
